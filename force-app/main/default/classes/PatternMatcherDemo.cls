public with sharing class PatternMatcherDemo {

    public void hasDuplicatesByDomain(Lead[] leads) {
        // This pattern reduces the email address to 'john@smithco' 
        // from 'john@*.smithco.com' or 'john@smithco.*'
     Pattern emailPattern = Pattern.compile('(?<=@)((?![\\w]+\\.[\\w]+$)[\\w]+\\.)|(\\.[\\w]+$)');
        // Define a set for emailkey to lead:
     Map<String,Lead> leadMap = new Map<String,Lead>();
             for(Lead lead:leads) {
                 // Ignore leads with a null email
                 if(lead.Email != null) {
                        // Generate the key using the regular expression
                    String emailKey = emailPattern.matcher(lead.Email).replaceAll('');
                        // Look for duplicates in the batch
                    if(leadMap.containsKey(emailKey)) 
                         lead.email.addError('Duplicate found in batch');
                    else {
                        // Keep the key in the duplicate key custom field
                         lead.Duplicate_Key__c = emailKey;
                         leadMap.put(emailKey, lead);
                    }
              }
          }
             // Now search the database looking for duplicates 
             for(Lead[] leadsCheck:[SELECT Id, duplicate_key__c FROM Lead WHERE 
             duplicate_key__c IN :leadMap.keySet()]) {
            for(Lead lead:leadsCheck) {
            // If there's a duplicate, add the error.
                if(leadMap.containsKey(lead.Duplicate_Key__c)) 
                   leadMap.get(lead.Duplicate_Key__c).email.addError('Duplicate found 
                      in salesforce(Id: ' + lead.Id + ')');
         }
     }
 }
}
